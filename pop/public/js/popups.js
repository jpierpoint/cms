function displayDialog(dialogTitle, msg) {
	/*
	$("#popupDialog").html('');
	$("#popupDialog").html(msg);
	$("#popupDialog").attr("class",dialogTitle);
	$("#popupDialog").dialog({
		modal: true
		});
		*/
	if (dialogTitle.indexOf('Error') != -1) {
		$.gritter.add({
			// (string | mandatory) the heading of the notification
			title: dialogTitle,
			// (string | mandatory) the text inside the notification
			text: msg,
			// (string | optional) the image to display on the left
			image: '/images/alert.png',
			// (bool | optional) if you want it to fade out on its own or just sit there
			sticky: false,
			// (int | optional) the time you want it to be alive for before fading out
			time: 3000
		});
	} else {
		$.gritter.add({
			// (string | mandatory) the heading of the notification
			title: dialogTitle,
			// (string | mandatory) the text inside the notification
			text: msg,
			// (string | optional) the image to display on the left
			image: '/images/checkmark.png',
			// (bool | optional) if you want it to fade out on its own or just sit there
			sticky: false,
			// (int | optional) the time you want it to be alive for before fading out
			time: 3000
		});
	}


}

$(document).ready(function() {
	$( "input[type=submit], a, button" ).button();
});