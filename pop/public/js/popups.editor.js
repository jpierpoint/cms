function updateJsonPopupRules() {
	window.jsonRules = [];
	$('#popupRulesSelect option').each(function() {
		window.jsonRules.push($(this).val());
	});
	$('#popup_rules_json').val('[' + window.jsonRules.join(',') + ']');	
}

$(document).ready(function() {
	$("#popupEditor").cleditor({
		width: 685,
		height: 685
		});
	$("#start_date").datetimepicker();
	$("#end_date").datetimepicker();
	
	popupRules = JSON.parse(window.encodedRules);
	if (popupRules.length > 0) {
		for (i = 0; i < popupRules.length; i++) {
			rulestoadd = new Object;
			rulestoadd.type = popupRules[i].type;
			rulestoadd.verb = popupRules[i].verb;
			rulestoadd.entity_id = popupRules[i].entity_id;
			if (rulestoadd.type == 'event') {
				$('#popupRulesSelect').append("<option value='" + JSON.stringify(rulestoadd) + "'>" + popupRules[i].verb + ' ' + popupRules[i].type + ' ' + popupRules[i].program_name + " (" + popupRules[i].event_location + ")</option>");
			} else {
				$('#popupRulesSelect').append("<option value='" + JSON.stringify(rulestoadd) + "'>" + popupRules[i].verb + ' ' + popupRules[i].type + ' ' + popupRules[i].program_name + "</option>");
			}
			
		}
	}
	$('#popup_rules_json').val(window.encodedRules);
	
	if (window.popupBackgroundColor) {
		defaultColorPickerColor = window.popupBackgroundColor;
	} else {
		defaultColorPickerColor = "#fff";
	}
	$('#background_color').spectrum({
		color: defaultColorPickerColor,
		preferredFormat: "hex",
		showInput: true
		});
	$('button.sp-choose').click(function () {
		window.popupBackgroundColor = $('#background_color').val();
		$('div.cleditorMain iframe').css('background-color',window.popupBackgroundColor);
		$('#background_image').val(0);
		$('div.cleditorMain iframe').css('background-image',"url('/images/trans.png')");
		$('div.popupBackgroundImage').css('border','2px solid #AAAAAA');
	});
	$('.sp-replacer').click(function() {
		$('#backgroundImages').hide();
	});
	$("#savePopup").click(function() {
		$("#loadingBackground").show();
		data = $("#popup").serialize();
		$.ajax({
			url: '/popups/ajax_save/' + window.popupID,
			data: data,
			dataType: 'json',
			type: 'POST',
			async: false,
			success: function(data) {
				$("#loadingBackground").hide();
				if (data.response === "success") {
					displayDialog('Save Successful','Saved version ' + data.data.version + ' of \'<b>' + $('#popupLabel').val() + '</b>\'');
					$("#popupVersionPicker").prepend("<option value='" + data.data.version + "'>" + data.data.version + " (" + data.data.created + ")</option>");
					$("#popupVersionPicker").val(data.data.version);
				} else {
					errorString = "";
					for (var i=0; i < data.errors.length; i++) {
						errorString += data.errors[i] + "<br />";
					}
					displayDialog('Error Saving','<b>Error:</b><br />' + errorString);
				}
				return false;
			},
			error: function(data) {
				$("#loadingBackground").hide();
				displayDialog('Application Error','Application Error');
				//$("#popupErrors").html('Application error - please check connection and try again.');
			}
		});
		return false;
	});
	$("#popupVersionPicker").change(function() {
		$("#loadingBackground").show();
		$.ajax({
			url: '/popups/ajax_get_version/' + window.popupID + "/" + $(this).val(),
			type: 'POST',
			dataType: 'json',
			async: false,
			success: function(data) {
				$("#loadingBackground").hide();
				if (data.response === "success") {
					displayDialog('Loaded Version','Loaded version ' + data.data.version + ' of \'<b>' + data.data.label + '</b>\'');
					$("#popupEditor").val(data.data.content).blur();
					$('div.cleditorMain iframe').css('background-image',"url('" + data.data.background_url + "')");
					$('#background_image').val(data.data.popup_background_id);
					$('div.popupBackgroundImage').css('border','2px solid #AAAAAA');
					$('#popupBackgroundImage_'+data.data.popup_background_id).css('border','2px solid #51a511');
					if (data.data.background_color) {
						$('div.cleditorMain iframe').css('background-color',data.data.background_color);
					}
					$('#background_color').val(data.data.background_color);
					window.popupBackgroundColor = data.data.background_color;
					$('#background_color').spectrum({
						color: data.data.background_color,
						preferredFormat: "hex",
						showInput: true
						});
					$('#text_color').val(data.data.text_color);
				} else {
					errorString = "";
					for (var i=0; i < data.errors.length; i++) {
						errorString += data.errors[i] + "<br />";
					}
					displayDialog('Error Selecting Version','<b>Error:</b><br />' + errorString);
				}
				return false;
			},
			error: function(data) {
				$("#loadingBackground").hide();
				displayDialog('Application Error','Application error - please check connection and try again.');
			}
		});
	});
	$('#background_color').focus(function() {
		$('#background_color').blur();
		$('#backgroundImages').hide();
	});
	$("#imagepicker").click(function() {
		$('#backgroundImages').toggle();
		if (window.popupBackgroundColor) {
			defaultColorPickerColor = window.popupBackgroundColor;
		} else {
			defaultColorPickerColor = "#fff";
		}
		$('#background_color').spectrum({
			color: defaultColorPickerColor,
			preferredFormat: "hex",
			showInput: true
			});
	});
	$('html').click(function() {
		$('#backgroundImages').hide();
	});
	$('#imagepicker, div.popupBackgroundImage').click(function(event) {
		event.stopPropagation();
	});
	$('.popupBackgroundImage').click(function() {
		$('#background_image').val($(this).attr('data-background-id'));
		$('div.cleditorMain iframe').css('background-image',"url('" + $(this).attr('data-background-image') + "')");
		$('div.popupBackgroundImage').css('border','2px solid #AAAAAA');
		$(this).css('border','2px solid #51a511');
	});
	if ($('#background_image').val()) {
		$('div.cleditorMain iframe').css('background-image',"url('" + window.popupBackgroundImage + "')");
		$('#popupBackgroundImage_'+$('#background_image').val()).css('border','2px solid #51a511');
	}
	if (window.popupBackgroundColor) {
		$('div.cleditorMain iframe').css('background-color',window.popupBackgroundColor);
	}
	$('#previewPopup').click(function() {
		window.open('/popups/preview/' + window.popupID + '/' + $("#popupVersionPicker").val(), '_blank', 'width=700, height=760');
	});
	$('#duplicatePopup').click(function() {
		window.location = "/popups/duplicate/" + window.popupID + "/" + $("#popupVersionPicker").val();
	});
	$("#deletePopup").click(function() {
		if (window.confirm("Are you sure you want to delete this popup?")) {
			window.location = "/popups/delete/" + window.popupID;
		}
	});
	$('#popupRules').click(function() {
		$('#popupRulesPanel').dialog({
			title: 'Popup Rules',
			show: { effect: "fade", duration: 300 },
			modal: false,
			width: 450,
			height: 340
			});
	});
	$('#popupAddRule').click(function() {
		rulestoadd = new Object;
		rulestoadd.type = $('input[name=popup_rules_type]:checked').val();
		rulestoadd.verb = $('#popupRulesHasHasnt').val();
		
		if ($('input[name=popup_rules_type]:checked').val() == 'program') {
			rulestoadd.entity_id = $('#popupRulesPrograms').val();
			ruleLabel = rulestoadd.verb + ' program ' + $('#popupRulesPrograms option:selected').text();
		} else {
			rulestoadd.entity_id = $('#popupRulesEvents').val();
			ruleLabel = rulestoadd.verb + ' event ' + $('#popupRulesEvents option:selected').text();
		}
		$('#popupRulesSelect').append("<option value='" + JSON.stringify(rulestoadd) + "'>" + ruleLabel + "</option>");
		updateJsonPopupRules();
	});
	$('#popupNixRule').click(function() {
		$('#popupRulesSelect option:selected').remove();
		updateJsonPopupRules();
	});
});