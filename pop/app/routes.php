<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/popups/manager', array('as' => 'manager',
	'uses' => 'PopupController@manager')
	);

Route::get('/popups/editor/{popup_id}', array('as' => 'editor',
	'uses' => 'PopupController@editor')
	);

Route::any('/popups/ajax_save/{popup_id}', array('as' => 'ajax_save',
	'uses' => 'PopupController@ajax_save')
	);

Route::any('/popups/ajax_get_version/{popup_id}/{version}', array('as' => 'ajax_get_version',
	'uses' => 'PopupController@ajax_get_version')
	);

Route::any('/popups/delete/{popup_id}', array('as' => 'delete',
	'uses' => 'PopupController@deletePopup')
	);

Route::any('/popups/duplicate/{popup_id}/{version}', array('as' => 'duplicate',
	'uses' => 'PopupController@duplicate')
	);
	
Route::get('/popups/editor', function() {
	return Redirect::route('manager');
	});
	
Route::get('/', function() {
	return Redirect::route('manager');
	});

Route::get('/popups', function() {
	return Redirect::route('manager');
	});

Route::any('/(.*)', function( $page ){
	return Redirect::route('manager');
	//dd($page);
	});