<?php

class PopupController extends BaseController {

	/*
	|--------------------------------------------------------------------------
	| Popup Controller
	|--------------------------------------------------------------------------
	|
	| Handles the listing of and editing of popups by their various versions
	|
	*/
	
	public function manager()
	{
		$popups = with(new Popup)->getAll();
		return View::make('manager')->with('popups', $popups);
	}
	
	public function editor($popup_id) {
		if ($popup_id == "new") {
			$popup_id = with(new Popup)->createNew();
		}
		$versions = with(new Popup)->getVersions($popup_id);
		$rules = json_encode(with(new Popup)->getRules($popup_id));

		$backgrounds = with(new Popup)->getBackgrounds();
		$programs = with(new Popup)->getPrograms();
		$events = with(new Popup)->getEvents();
		return View::make('editor')->with('versions', $versions)
				->with('backgrounds', $backgrounds)
				->with('events', $events)
				->with('programs', $programs)
				->with('rules', $rules);
	}
	
	public function ajax_save($popup_id) {
		$popupVersion = new PopupVersion($popup_id);
		
		if ($popupVersion->isValid) {
			$save = with(new Popup)->saveVersion($popupVersion);
			if (! $save) {
				$popupVersion->errors[] = "Error saving version";
			}
		}
		
		if (count($popupVersion->errors)) {
			$response = json_encode(array(
					'response' => 'failure',
					'errors' => $popupVersion->errors
					));
		} else {
			$response = json_encode(array(
				       "response" => "success",
				       "data" => $save[0]));
		}
		
		return View::make('ajax_save')->with('response', $response);
	}
	
	public function ajax_get_version($popup_id, $version) {
		$popup_id = (int) $popup_id;
		$version = (int) $version;
		$popup = false;
		$errors = array();
		
		if (! $popup_id)
			$errors[] = "Invalid content ID";

		if ($version !== 0 && ! $version)
			$errors[] = "Invalid version";
		
		if (! count($errors))
			$popup = with(new Popup)->getVersion($popup_id, $version);
		
		if (! $popup)
			$errors[] = "Unable to retrieve popup version";
	
		if (count($errors)) {
			$response = json_encode(array(
				"response" => "failure",
				"errors" => $errors
			));
		} else {
			$response = json_encode(array(
				"response" => "success",
				"data" => $popup));
		}
		
		return View::make('ajax_save')->with('response', $response);
	}
	
	public function deletePopup($popup_id) {
		with(new Popup)->deletePopup($popup_id);
		
		return Redirect::route('manager');
	}
	
	public function duplicate($popup_id, $version) {
		$copy = with(new Popup)->duplicate($popup_id, $version);
		if ($copy) {
			return Redirect::to("/popups/editor/{$copy->popup_id}");
		} else {
			return Redirect::route('manager');
		}
	}

}