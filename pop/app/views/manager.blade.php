@extends('layouts.default')

@section('head_css')
	<link rel="stylesheet" type="text/css" href="/css/popups.css" />
	<link rel="stylesheet" type="text/css" href="/css/jquery.dataTables.css" />
	<link rel="stylesheet" type="text/css" href="/css/south-street/jquery-ui-1.10.4.custom.min.css" />
	<link rel="stylesheet" type="text/css" href="/css/popups.css" />
@stop

@section('head_js')
	<script type="text/javascript" src="/js/jquery.js"></script>
	<script type="text/javascript" src="/js/jquery.dataTables.js"></script>
	<script type="text/javascript" src="/js/jquery-ui-1.10.4.custom.min.js"></script>
	<script type="text/javascript" src="/js/popups.js"></script>
	<script type="text/javascript" language="javascript" class="init">
	$(document).ready(function() {
		var table = $('#popups').dataTable();
		$("#popups tbody").delegate('tr', 'click', function() {
			window.location = "/popups/editor/" + $(this).attr("data-popup-id");
			});
	});
	</script>
@stop

@section('content')
	<h2>
		Popups
	</h2>
	
	<table id="popups" class="display" cellspacing="0" width="100%">
		<thead>
			<tr>
				<th>Name</th>
				<th>Priority</th>
				<th>Max Views</th>
				<th>Day Break</th>
				<th>Status</th>
				<th>Start</th>
				<th>End</th>
				<th>Created</th>
			</tr>
		</thead>
		
		<tbody>
			@foreach($popups as $popup)
				<tr class="popupRow{{{ $popup->status == 1 ? '' : ' inactivePopup' }}}" data-popup-id="{{ $popup->popup_id }}">
					<td>{{ $popup->label }}</td>
					<td>{{ $popup->priority }}</td>
					<td>{{ $popup->max_views }}</td>
					<td>{{ $popup->days_break }}</td>
					<td>{{{ $popup->status == 1 ? 'On' : 'Off' }}}</td>
					<td><?php echo ($popup->start_date ? DateTime::createFromFormat('Y-m-d H:i:s',$popup->start_date)->format('M j, Y') : ''); ?></td>
					<td><?php echo ($popup->end_date ? DateTime::createFromFormat('Y-m-d H:i:s',$popup->end_date)->format('M j, Y') : ''); ?></td>
					<td><?php echo DateTime::createFromFormat('Y-m-d H:i:s',$popup->created)->format('M j, Y h:m');?></td>
				</tr>
			@endforeach
		</tbody>
	</table>
@stop