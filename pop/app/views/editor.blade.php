@extends('layouts.default')

@section('head_css')
	<link rel="stylesheet" type="text/css" href="/css/jquery.cleditor.css" />
	<link rel="stylesheet" type="text/css" href="/css/south-street/jquery-ui-1.10.4.custom.min.css" />
	<link rel="stylesheet" type="text/css" href="/css/popups.css" />
	<link rel="stylesheet" type="text/css" href="/css/jquery.gritter.css" />
	<link rel="stylesheet" type="text/css" href="/css/jquery-ui-timepicker-addon.css" />
	<link rel="stylesheet" type="text/css" href="/css/spectrum.css" />
@stop

@section('head_js')
	<script type="text/javascript" src="/js/jquery.js"></script>
	<script type="text/javascript" src="/js/jquery.cleditor.min.js"></script>
	<script type="text/javascript" src="/js/jquery-ui-1.10.4.custom.min.js"></script>
	<script type="text/javascript" src="/js/popups.js"></script>
	<script type="text/javascript" src="/js/popups.editor.js"></script>
	<script type="text/javascript" src="/js/jquery.gritter.min.js"></script>
	<script type="text/javascript" src="/js/jquery-ui-timepicker-addon.js"></script>
	<script type="text/javascript" src="/js/spectrum.js"></script>
		
	<script type="text/javascript">
		window.popupID = {{ $versions[0]->popup_id }};
		window.popupBackgroundID = {{{ $versions[0]->popup_background_id ? $versions[0]->popup_background_id : '0' }}};
		window.popupBackgroundImage = '{{ $versions[0]->background_url }}';
		window.popupBackgroundColor = '{{ $versions[0]->background_color }}';
		window.encodedRules = '{{ $rules }}';
		window.jsonRules = [];
	</script>
@stop

@section('content')

<form id="popup" onsubmit="return false;">
	<input type="hidden" name="popup_rules_json" id="popup_rules_json" />
	<div class="popupPanel">
		<table>
			<tr>
				<td class="hdr">
					Label:
				</td>
				<td class="dta">
					<input type="text" name="label" id="popupLabel" value="{{ $versions[0]->label }}" />
				</td>
			</tr>
			<tr>
				<td></td>
				<td class="dsc">
					Internal label for popup identification
				</td>
			</tr>
			<tr>
				<td class="hdr">
					Version:
				</td>
				<td class="dta">
					<select id="popupVersionPicker">
					@foreach ($versions as $version)
						<option value="{{ $version->version }}">{{ $version->version }} ({{ $version->created }})</option>
					@endforeach
					</select>
				</td>
			</tr><tr>
				<td></td>
				<td class="dsc">
					Previously saved versions
				</td>
			</tr>
			<tr>
				<td class="hdr">
					Status:
				</td>
				<td class="dta">
					<select id="status" name="status">
						<option value="0" {{{ $versions[0]->status == 1 ? '' : 'SELECTED' }}}>Inactive</option>
						<option value="1" {{{ $versions[0]->status == 1 ? 'SELECTED' : '' }}}>Active</option>
					</select>
				</td>
			</tr>
			<tr>
				<td></td>
				<td class="dsc">
					Whether this popup is on or off
				</td>
			</tr>
			<tr>
				<td class="hdr">
					Background:
				</td>
				<td class="dta">
					<div id="backgroundPicker">
						<input type="text" name="background_color" id="background_color" />
						
						<button id="imagepicker"></button>
						<input type="hidden" name="background_image" id="background_image" value="{{{ $versions[0]->background ? $versions[0]->background : '0' }}}" />
						<div id="backgroundImages">
							<div class="popupBackgroundImage" data-background-id="0" data-background-image="/images/trans.png" style="background-color: #FFFFFF; font-weight: bold; height: 35px; padding-top: 15px; text-align: center;">
								N/A
							</div>
							@foreach ($backgrounds as $background)
								<div class="popupBackgroundImage" id="popupBackgroundImage_{{ $background->popup_background_id }}" data-background-id="{{ $background->popup_background_id; }}" data-background-image="{{ $background->background_url }}" style="background-image: url('{{ $background->background_url }}');">
								&nbsp;
								</div>
							@endforeach
						</div>
					</div>
				</td>
			</tr>
			<tr>
				<td class="hdr">
					Text:
				</td>
				<td class="dta">
					<select name="text_color" id="text_color">
						<option value="#000" {{{ $versions[0]->text_color == '#000' ? 'SELECTED' : '' }}}>Black</option>
						<option value="#FFF" {{{ $versions[0]->text_color == '#FFF' ? 'SELECTED' : '' }}}>White</option>
					</select>
				</td>
			</tr>
			<tr>
				<td></td>
				<td class="dsc">
					The background image/color and default text color for the popup
				</td>
			</tr>
			<tr>
				<td class="hdr">
					Display from:
				</td>
				<td class="dta">
					<input id="start_date" name="start_date" type="text" value="<?php echo ($versions[0]->start_date ? DateTime::createFromFormat('Y-m-d H:i:00',$versions[0]->start_date)->format('m/d/Y H:i') : date('m/d/Y H:i',time())); ?>" />
					to</b>
					<input id="end_date" name="end_date" type="text" value="<?php echo ($versions[0]->end_date ? DateTime::createFromFormat('Y-m-d H:i:00',$versions[0]->end_date)->format('m/d/Y H:i') : ''); ?>" />
				</td>
			</tr>
			<tr>
				<td></td>
				<td class="dsc">
					When to start/end the popup (leave blank for no end date)
				</td>
			</tr>
			<tr>
				<td class="hdr">
					Priority:
				</td>
				<td class="dta">
					<input type="text" name="priority" id="popupPriority" value="{{{ $versions[0]->priority ? $versions[0]->priority : '0' }}}" />
				</td>
			</tr>
			<tr>
				<td></td>
				<td class="dsc">
					The order in which to display the popup when multiple are active
				</td>
			</tr>
			<tr>
				<td class="hdr">
					Days Break:
				</td>
				<td class="dta">
					<input type="text" name="days_break" id="days_break" value="{{{ $versions[0]->days_break ? $versions[0]->days_break : '0' }}}" />
				</td>
			</tr>
			<tr>
				<td></td>
				<td class="dsc">
					Days to wait to display another popup after this one
				</td>
			</tr>
			<tr>
				<td class="hdr">
					Max Views:
				</td>
				<td class="dta">
					<input type="text" name="max_views" id="max_views" value="{{{ $versions[0]->max_views ? $versions[0]->max_views : '0' }}}" />
				</td>
			</tr>
			<tr>
				<td></td>
				<td class="dsc">
					Maximum number of times to display this popup
					<br />(multiple views in 1 day count as 1)
				</td>
			</tr>
			<tr>
				<td class="hdr">
					Rules:
				</td>
				<td class="dta">
					<button id="popupRules" style="background-image: url('/images/trans.png'); background-color: #237adc; border: 1px solid #237adc;color: #fff;">View/Edit</button>
				</td>
			</tr>
			<tr>
				<td></td>
				<td class="dsc">
					Only show if user has/has not purchased X product(s)
				</td>
			</tr>
		</table>
		<br />
		<button id="savePopup">Save</button>
		<button id="duplicatePopup">Duplicate</button>
		<button id="deletePopup" style="color: #fff; background-image: url('/images/trans.png'); background-color: red; border: 1px solid red;">Delete</button>
	</div>
	
	<div class="popupPanel">
		<textarea id="popupEditor" name="content">
			{{ $versions[0]->content }}
		</textarea>
		<br /><br />
	</div>

	<div id="popupRulesPanel">
		<i style="font-size: 12px;">Only display this popup if the member: &nbsp;
					<select id="popupRulesHasHasnt">
						<option value="Has">Has</option>
						<option value="Needs">Needs</option>
					</select></i>
		<table border="0">
			<tr>
				<td colspan="2">
					
				</td>
			</tr><tr>
				<td>
					<input name="popup_rules_type" type="radio" value="program" checked />
				</td><td>
					<select id="popupRulesPrograms" style="font-size: 12px;">
						@foreach($programs as $program)
							<option value="{{ $program->program_id }}">{{ $program->program_name }}</option>
						@endforeach
					</select>
				</td>
			</tr><tr>
				<td>
					<input name="popup_rules_type" type="radio" value="event" />
				</td><td>
					<select id="popupRulesEvents" style="font-size: 12px;">
						@foreach($events as $event)
							<option value="{{ $event->event_id }}">{{ $event->event_name }} ({{ $event->event_location }})</option>
						@endforeach
					</select>
				</td>
			</tr><tr>
				<td colspan="2" align="center">
					<button id="popupAddRule">Add Rule</button>
					<br /><br />
				</td>
			</tr><tr>
				<td colspan="2">
					<select multiple id="popupRulesSelect" name="popup_rules" style="width: 100%; font-size: 12px;">
						<!--<option>Add rules above...</option>-->
					</select>
				</td>
			</tr><tr>
				<td colspan="2" align="center">
					<button id="popupNixRule" style="color: #fff; background-image: url('/images/trans.png'); background-color: red; border: 1px solid red;">Remove Rule</button>
				</td>
			</tr>
		</table>
		
	</div>
</form>

@stop