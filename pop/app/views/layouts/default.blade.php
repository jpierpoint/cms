<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>LaraPop</title>
	
        <link rel="shortcut icon" href="/cakepop.ico">
        
	@yield('head_css')
	@yield('head_js')
</head>
<body>
    <div id="container">
        <div id="header">
                <h1>lara<span style='color:#cc1aa9;'>pop</span></h1>
        </div>
        <div id="navigation">
            <ul id="navigationMenu">
                <li>
                    <a href="/popups/manager">Manage Popups</a>
                </li>
                <li>
                    <a href="/popups/editor/new">New Popup</a>
                </li>
            </ul>
        </div>
        
    @yield('content')
    
    </div>
</body>
</html>
