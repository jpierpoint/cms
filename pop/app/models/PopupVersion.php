<?php

use Illuminate\Support\Facades\Facade;

class PopupVersion extends Facade {
	public $popup_id;
	public $content;
	public $label;
	public $priority;
	public $maxViews;
	public $daysBreak;
	public $status;
	public $startDate;
	public $endDate;
	public $background;
	public $backgroundColor;
	public $textColor;
	public $currentVersion;
	public $jsonRules;
	public $isValid;
	public $errors;
	
	function PopupVersion(
			      $popup_id = false, $content = false, $label = false, $priority = false, $maxViews = false,
			      $daysBreak = false, $status = false, $startDate = false, $endDate = false, $background = false,
			      $backgroundColor = false, $textColor = false, $currentVersion = false, $jsonRules = false
			      ) {
		
		$this->errors = array();
		$this->isValid = false;
		
		/***
		 *
		 * 	Populate data for popup version, with priority for passed parameters
		 * 	falling back to POST/GET data
		 * 
		 ***/
		
		if ($popup_id)
			$this->popup_id = $popup_id;
		else
			$this->popup_id = Input::get('popup_id');

		if ($content)
			$this->content = $content;
		else
			$this->content = Input::get('content');
		
		if  ($label)
			$this->label = $label;
		else
			$this->label = Input::get('label');
		
		if ($priority)
			$this->priority = $priority;
		else
			$this->priority = (int) Input::get('priority');
		
		if ($maxViews)
			$this->maxViews = $maxViews;
		else
			$this->maxViews = (int) Input::get('max_views');
		
		if ($daysBreak)
			$this->daysBreak = $daysBreak;
		else
			$this->daysBreak = (int) Input::get('days_break');
		
		if ($status)
			$this->status = $status;
		else
			$this->status = (int) Input::get('status');
		
		if ($startDate)
			$this->startDate = $startDate;
		else
			$this->startDate = Input::get('start_date') . (Input::get('start_date') === "" ? "" : ":00");
		
		if ($endDate)
			$this->endDate = $endDate;
		else
			$this->endDate = Input::get('end_date') . (Input::get('end_date') === "" ? "" : ":00");
		
		if ($background)
			$this->background = $background;
		else
			$this->background = (int) Input::get('background_image');
		
		if ($backgroundColor)
			$this->backgroundColor = $backgroundColor;
		else
			$this->backgroundColor = Input::get('background_color');
		
		if ($textColor)
			$this->textColor = $textColor;
		else
			$this->textColor = Input::get('text_color');
		
		if ($currentVersion)
			$this->currentVersion = $currentVersion;
		else
			$this->currentVersion = (int) Input::get('current_version');
		
		if ($jsonRules)
			$this->jsonRules = $jsonRules;
		else
			$this->jsonRules = Input::get('popup_rules_json');
		
		/***
		 *
		 * 	Check integrity of popup version and catalog any errors
		 * 
		 ***/
		
		if (! $this->popup_id)
			$this->errors[] = "Invalid content ID";

		if (! $this->content)
			$this->errors[] = "No content provided";

		if (! $this->label)
			$this->errors[] = "No popup label provided";
		
		if (! $this->maxViews)
			$this->maxViews = 0;

		if (! $this->daysBreak)
			$this->daysBreak = 0;

		if (! $this->status)
			$this->status = 0;

		if (! $this->textColor)
			$this->textColor = "#000";
		
		if (DateTime::createFromFormat('m/d/Y H:i:00', $this->startDate) === false && $this->startDate !== "")
			$this->errors[] = "Invalid format for start date: '{$this->startDate}'";

		if (DateTime::createFromFormat('m/d/Y H:i:00', $this->endDate) === false && $this->endDate !== "")
			$this->errors[] = "Invalid format for end date: '{$this->endDate}'";
		
		if ($this->label && ! with(new Popup)->checkUniqueLabel($this->popup_id, $this->label))
			$this->errors[] = "Popup label must be unique";
		
		if (! count($this->errors))
			$this->isValid = true;
	}
}