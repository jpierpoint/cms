<?php

class Popup extends Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'popups';
	
	public function getAll() {
		return DB::select( DB::raw("SELECT * FROM popups Popup
						WHERE label IS NOT NULL
						AND deleted IS NULL
						ORDER BY status DESC, priority ASC") );
	}
	
	public function getVersions($popup_id) {
		return DB::select( DB::raw("SELECT * FROM popups Popup
						JOIN popup_versions PopupVersion ON Popup.popup_id = PopupVersion.popup_id
						JOIN popup_backgrounds PopupBackground ON PopupVersion.background = PopupBackground.popup_background_id
						WHERE Popup.popup_id = :popup_id
						ORDER BY PopupVersion.version DESC"), array(
					'popup_id' => $popup_id) );
	}
	
	public function getBackgrounds() {
		return DB::select( DB::raw("SELECT * FROM popup_backgrounds PopupBackground
					    WHERE active = 1
					    AND popup_background_id <> 0") );
	}
	
	public function getPrograms() {
		return DB::select( DB::raw("SELECT program_id, program_name FROM program
					    ORDER BY program_id ASC") );
	}
	
	public function getEvents() {
		return DB::select( DB::raw("SELECT * FROM event
					    WHERE event_status = 1
					    OR event_status = 2
					    ORDER BY event_id DESC") );
	}
	
	public function getRules($popup_id) {
		return DB::select( DB::raw("SELECT * FROM
						(SELECT pr.popup_id, pr.verb, pr.type, pr.entity_id, p.program_name, NULL event_location
						FROM popup_rules pr
							LEFT JOIN program p ON pr.entity_id = p.program_id
							WHERE pr.type = 'program'
						UNION ALL
						SELECT pr.popup_id, pr.verb, pr.type, pr.entity_id, e.event_name, e.event_location event_location
						FROM event e
							LEFT JOIN popup_rules pr
							ON pr.entity_id = e.event_id
							WHERE pr.type = 'event'
						) X
						WHERE popup_id = :popup_id"), array(
			'popup_id' => $popup_id) );
	}
	
	public function createNew() {
		$rs = DB::statement( DB::raw("INSERT INTO popups (created)
					      VALUES (CURRENT_TIMESTAMP())") );
		if (! $rs)
			return false;
		
		$popup = DB::select( DB::raw("SELECT popup_id FROM popups
					      WHERE label IS NULL
					      ORDER BY popup_id DESC LIMIT 1") );
		if (! $popup)
			return false;
		
		$rs = DB::statement( DB::raw("INSERT INTO popup_versions (popup_id,version,created)
					      VALUES (:popup_id,0,CURRENT_TIMESTAMP())"), array(
					'popup_id' => $popup[0]->popup_id) );
		if (! $rs)
			return false;
		
		return $popup[0]->popup_id;
	}
	
	public function checkUniqueLabel($popup_id, $label) {
		$rs = DB::select( DB::raw("SELECT COUNT(*) PopupLabelCount FROM popups
					   WHERE label = :label
						AND popup_id <> :popup_id
						AND deleted IS NULL"), array(
					'popup_id' => $popup_id,
					'label' => $label) );
		if (isset($rs[0]->PopupLabelCount) && $rs[0]->PopupLabelCount > 0) {
			return false;
		} else {
			return true;
		}
	}
	
	public function getVersion($popup_id, $version) {
		$version = DB::select( DB::raw("SELECT * FROM popups Popup
						JOIN popup_versions PopupVersion ON Popup.popup_id = PopupVersion.popup_id
						JOIN popup_backgrounds PopupBackground ON PopupVersion.background = PopupBackground.popup_background_id
						WHERE Popup.popup_id = :popup_id AND PopupVersion.version = :version LIMIT 1"), array(
					'popup_id' => $popup_id,
					'version' => $version ) );
		if (! isset($version[0]))
			return false;
		return $version[0];
	}
	
	public function saveVersion($popupVersion) {
		$popupVersion->startDate = ($popupVersion->startDate === "" ? "" : DateTime::createFromFormat('m/d/Y H:i:00',$popupVersion->startDate)->format('Y-m-d H:i:s'));
		$popupVersion->endDate = ($popupVersion->endDate === "" ? "" : DateTime::createFromFormat('m/d/Y H:i:00',$popupVersion->endDate)->format('Y-m-d H:i:s'));
		$rs = DB::statement( DB::raw("UPDATE popups SET
						label = :label,
						priority = :priority,
						max_views = :max_views,
						days_break = :days_break,
						start_date = :start_date,
						end_date = :end_date,
						status = :status
					      WHERE popup_id = :popup_id"), array(
					'popup_id' => $popupVersion->popup_id,
					'label' => $popupVersion->label,
					'priority' => $popupVersion->priority,
					'max_views' => $popupVersion->maxViews,
					'days_break' => $popupVersion->daysBreak,
					'start_date' => $popupVersion->startDate,
					'end_date' => $popupVersion->endDate,
					'status' => $popupVersion->status
					) );
		if (! $rs)
			return false;
		
		$nextVersion = DB::select( DB::raw("SELECT MAX(version) CurrentVersion FROM popup_versions PopupVersion
						    WHERE popup_id = :popup_id"), array(
						'popup_id' => $popupVersion->popup_id) );
		if (! isset($nextVersion[0]->CurrentVersion))
			return false;
		
		$rs = DB::statement( DB::raw("INSERT INTO popup_versions (popup_id,content,version,background,background_color,text_color,created)
					      VALUES (:popup_id, :content, :version, :background, :background_color, :text_color, CURRENT_TIMESTAMP())"), array(
					'popup_id' => $popupVersion->popup_id,
					'content' => $popupVersion->content,
					'version' => $nextVersion[0]->CurrentVersion + 1,
					'background' => $popupVersion->background,
					'background_color' => $popupVersion->backgroundColor,
					'text_color' => $popupVersion->textColor,
					) );
		if (! $rs)
			return false;
		
		$savedVersion = DB::select( DB::raw("SELECT * FROM popup_versions PopupVersion
						     WHERE popup_id = :popup_id
							AND version = :version LIMIT 1"), array(
						'popup_id' => $popupVersion->popup_id,
						'version' => $nextVersion[0]->CurrentVersion + 1
						) );
		
		$rs = DB::statement( DB::raw("DELETE FROM popup_rules WHERE popup_id = :popup_id"), array(
						'popup_id' => $popupVersion->popup_id ) );
		$rules = json_decode($popupVersion->jsonRules);
		//var_dump($rules);exit();
		if (is_array($rules)) {
			foreach($rules as $each_rule) {
				$rs = DB::statement( DB::raw(	"INSERT INTO popup_rules (popup_id, verb, type, entity_id)
								 VALUES (:popup_id, :verb, :type, :entity_id)"), array(
									'popup_id' => $popupVersion->popup_id,
									'verb' => $each_rule->verb,
									'type' => $each_rule->type,
									'entity_id' => $each_rule->entity_id
							) );
			}
		}
		
		return $savedVersion;
	}
	
	public function deletePopup($popup_id) {
		$rs = DB::statement( DB::raw("UPDATE popups SET deleted = CURRENT_TIMESTAMP()
					      WHERE popup_id = :popup_id"), array(
					'popup_id' => $popup_id) );
		if (! $rs)
			return false;
		return true;
	}
	
	public function duplicate($popup_id, $version) {
		$rs = DB::statement( DB::raw("INSERT INTO popups
					      SELECT NULL popup_id, CONCAT(label, ' copy') label, priority, body_cms_id, video, html, cta_html, max_views, days_break, status, start_date, end_date, CURRENT_TIMESTAMP(), NULL
						FROM popups WHERE popup_id = :popup_id"), array(
					'popup_id' => $popup_id) );
		
		if (! $rs)
			return false;
		
		$oldPopup = DB::select( DB::raw("SELECT * FROM popups
					      WHERE popup_id = :popup_id
					      ORDER BY popup_id DESC
					      LIMIT 1"), array(
					'popup_id' => $popup_id) );
		
		$popup = DB::select( DB::raw("SELECT * FROM popups
					      WHERE label = :label
					      ORDER BY popup_id DESC
					      LIMIT 1"), array(
					'label' => $oldPopup[0]->label . ' copy') );
		
		$popupVersion = DB::select( DB::raw("SELECT * FROM popup_versions
					        WHERE popup_id = :popup_id
						AND version = :version
						LIMIT 1"), array(
					'popup_id' => $popup_id,
					'version' => $version) );
		
		$rs = DB::statement( DB::raw("INSERT INTO popup_versions (popup_id, content, version, background, background_color, text_color, created)
					      VALUES (:popup_id, :content, 1, :background, :background_color, :text_color, CURRENT_TIMESTAMP())"), array(
					'popup_id' => $popup[0]->popup_id,
					'content' => $popupVersion[0]->content,
					'background' => $popupVersion[0]->background,
					'background_color' => $popupVersion[0]->background_color,
					'text_color' => $popupVersion[0]->text_color) );
		
		if (! $rs)
			return false;
		
		return $popup[0];
	}
}